package ca.ulaval.dti.odl.utils;

import java.io.Serializable;

/**
 * Définie une référence sur un objet. Cette interface permet de faire une indirection sur un
 * objet. L'objet référé peut donc changer au gré de l'implémenteur.
 *
 * @author Olivier Lafontaine
 *
 */
public interface Reference<T> extends Serializable {

    /**
     * @return L'objet référé. Cet objet peut changer dans le temps, il est donc recommander de ne pas conserver
     * l'instance retournée.
     */
    T get();
}
