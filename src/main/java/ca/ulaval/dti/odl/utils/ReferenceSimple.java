package ca.ulaval.dti.odl.utils;

/**
 * Implantation d'une référence sur un objet constant.
 *
 * @param <T> le type de l'objet référé
 *
 */
public class ReferenceSimple<T> implements Reference<T> {

    private final T target;

    public ReferenceSimple(T target) {
        this.target = target;
    }

    @Override
    public T get() {
        return target;
    }
}
