package ca.ulaval.dti.odl.utils;

import org.apache.commons.lang3.StringUtils;

public class ParamUtils {

    public static int obtenirCapaciteValidee(String capacite) {
        if (capacite != null && StringUtils.isNotEmpty(capacite) && StringUtils.isNotBlank(capacite) && StringUtils.isNumeric(capacite)) {
            return Integer.valueOf(capacite);
        } else {
            return 0;
        }
    }
}
