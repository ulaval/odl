package ca.ulaval.dti.odl;

import ca.ulaval.dti.odl.config.ConfigGloProperties;
import ca.ulaval.dti.odl.config.ConfigOAuthProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ConfigOAuthProperties configOAuthProperties;

    @Autowired
    private ConfigGloProperties configGloProperties;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.run();
    }

    public void run(String... args) throws Exception {
        System.out.println("OAUTH config: " + configOAuthProperties.getClientId());
        System.out.println("GLO config: " + configGloProperties.getCheminInventaireSalles());


    }
}