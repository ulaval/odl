package ca.ulaval.dti.odl.services;

import ca.ulaval.dti.odl.services.salles.SalleGlo;

import java.util.List;

/**
 * Interface pour le client du service d’inventaire des salles de GLO.
 * Il permet d'accéder à l'inventaire des lieux disponibles dans le module de réservation des locaux (MRL)
 */
public interface ClientServiceInventaireSalles {

    /**
     * Obtient l'ensemble de l'inventaire des salles.
     *
     * @return la liste des salles
     */
    List<SalleGlo> obtenirInventaire();

    String obtenirInventaireFormatJson();

}
