package ca.ulaval.dti.odl.services.salles;

import java.io.Serializable;
import java.util.List;

/**
 * API de GLO:
 * Représente une salle que l'on retrouve sur le campus. .
 *
 * La salle correspond à l'élément "Espaces de l'établissement - Salles" du MRL.
 * La salle provient de l'objet RoomComplete de l'API Enterprise.
 */

public class SalleGlo implements Serializable {

    /**
     * Identifiant de la salle (ex: PLT_1280)
     *
     * <h4>Implémentation</h4>
     * L'identifiant est composé du code de pavillon et du numéro de la salle, concaténés avec un souligné '_'.
     */
    private final String idSalle;

    /**
     * Numéro de la salle
     *
     * <h4>Implémentation</h4>
     * Provient de l'attribut RoomComplete.id
     */
    private final String numero;

    /**
     * Pavillon dans lequel est situé la salle
     *
     * <h4>Implémentation</h4>
     * Correspond au building associé au code RoomComplete.building
     */
    private final PavillonGlo pavillon;

    /**
     * Type de la salle
     *
     * <h4>Implémentation</h4>
     * Correspond au type de salle associé au code RoomComplete.roomType
     */
    private final TypeSalleGlo typeSalle;

    /**
     * Période effective de la salle
     *
     * <h4>Implémentation</h4>
     * Correspond à la période bornée par les dates RoomComplete.effectiveStart et RoomComplete.effectiveEnd
     */
    private final PeriodeEffectiveGlo periodeEffective;

    /**
     * Capacité par défaut de la salle
     *
     * <h4>Implémentation</h4>
     * Provient de l'attribut RoomComplete.capacity
     */
    private final Integer capaciteParDefault;

    /**
     * Configurations particulière de la salle
     *
     * <h4>Implémentation</h4>
     * Provient de l'attribut RoomComplete.eventRoomConfigurations
     */
    private final List<CapaciteSalleGlo> capacites;

    public SalleGlo(String idSalle,
                    String numero,
                    PavillonGlo pavillon,
                    TypeSalleGlo typeSalle,
                    PeriodeEffectiveGlo periodeEffective,
                    Integer capaciteParDefault,
                    List<CapaciteSalleGlo> capacites) {
        this.idSalle = idSalle;
        this.numero = numero;
        this.pavillon = pavillon;
        this.typeSalle = typeSalle;
        this.periodeEffective = periodeEffective;
        this.capaciteParDefault = capaciteParDefault;
        this.capacites = capacites;
    }

    private SalleGlo(Builder builder) {
        idSalle = builder.idSalle;
        numero = builder.numero;
        pavillon = builder.pavillon;
        typeSalle = builder.typeSalle;
        periodeEffective = builder.periodeEffective;
        capaciteParDefault = builder.capaciteParDefault;
        capacites = builder.capacites;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getIdSalle() {
        return idSalle;
    }

    public String getNumero() {
        return numero;
    }

    public PavillonGlo getPavillon() {
        return pavillon;
    }

    public TypeSalleGlo getTypeSalle() {
        return typeSalle;
    }

    public PeriodeEffectiveGlo getPeriodeEffective() {
        return periodeEffective;
    }

    public Integer getCapaciteParDefault() {
        return capaciteParDefault;
    }

    public List<CapaciteSalleGlo> getCapacites() {
        return capacites;
    }

    public static SalleGlo.Builder builder() {
        return new SalleGlo.Builder();
    }

    public static final class Builder {

        private String idSalle;
        private String numero;
        private PavillonGlo pavillon;
        private TypeSalleGlo typeSalle;
        private PeriodeEffectiveGlo periodeEffective;
        private Integer capaciteParDefault;
        private List<CapaciteSalleGlo> capacites;

        private Builder() {
        }

        public Builder withIdSalle(String idSalle) {
            this.idSalle = idSalle;
            return this;
        }

        public Builder withNumero(String numero) {
            this.numero = numero;
            return this;
        }

        public Builder withPavillon(PavillonGlo pavillon) {
            this.pavillon = pavillon;
            return this;
        }

        public Builder withTypeSalle(TypeSalleGlo typeSalle) {
            this.typeSalle = typeSalle;
            return this;
        }

        public Builder withPeriodeEffective(PeriodeEffectiveGlo periodeEffective) {
            this.periodeEffective = periodeEffective;
            return this;
        }

        public Builder withCapaciteParDefault(Integer capaciteParDefault) {
            this.capaciteParDefault = capaciteParDefault;
            return this;
        }

        public Builder withCapacites(List<CapaciteSalleGlo> capacites) {
            this.capacites = capacites;
            return this;
        }

        public SalleGlo build() {
            return new SalleGlo(this);
        }
    }
}
