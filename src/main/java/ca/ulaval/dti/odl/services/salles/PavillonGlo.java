package ca.ulaval.dti.odl.services.salles;

import java.io.Serializable;

/**
 * API de GLO: Représente un pavillon que l'on retrouve sur le campus.
 *
 * <h4>Implémentation: </h4>
 * Le pavillon correspond à l'élément "Espaces de l'établissement - Pavillons" du MRL.
 * Le pavillon provient de l'objet BuildingComplete de l'API Enterprise.
 */

public class PavillonGlo implements Serializable {

    /**
     * Identifiant du pavillon (ex: PLT, CSL)
     */
    private final String idPavillon;

    /**
     * Description textuelle du pavillon (ex: Adrien-Pouliot, Louis-Jacques-Casault)
     */
    private final String description;

    public PavillonGlo(String idPavillon, String description) {
        this.idPavillon = idPavillon;
        this.description = description;
    }

    public String getIdPavillon() {
        return idPavillon;
    }

    public String getDescription() {
        return description;
    }
}
