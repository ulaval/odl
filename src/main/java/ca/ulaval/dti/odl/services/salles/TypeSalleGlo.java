package ca.ulaval.dti.odl.services.salles;

import java.io.Serializable;

/**
 * API de GLO:
 * Représente un type de salle que l'on retrouve sur le campus.
 *
 * <h4>Implémentation: </h4>
 * Le type de salle correspond à l'élément "Attributs d'espace - Types de salles" du MRL.
 * Le type de salle provient de l'objet RoomTypeComplete de l'API Enterprise.
 */

public class TypeSalleGlo implements Serializable {

    /**
     * Identifiant du type de salle (ex: REUNI, LABIN)
     */
    private final String idTypeSalle;

    /**
     * Description textuelle du type (ex: Salle de réunion, Laboratoire informatique)
     */
    private final String description;

    public TypeSalleGlo(String idTypeSalle, String description) {
        this.idTypeSalle = idTypeSalle;
        this.description = description;
    }

    public String getIdTypeSalle() {
        return idTypeSalle;
    }

    public String getDescription() {
        return description;
    }
}
