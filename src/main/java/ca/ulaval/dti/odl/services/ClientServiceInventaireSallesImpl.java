package ca.ulaval.dti.odl.services;

import ca.ulaval.dti.odl.config.ClientOAuth2;
import ca.ulaval.dti.odl.config.ConfigClientServiceInventaireSalles;
import ca.ulaval.dti.odl.config.JetonOAuth;
import ca.ulaval.dti.odl.services.salles.SalleGlo;
import ca.ulaval.dti.odl.utils.Reference;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.util.Assert;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Implémentation HttpClient pour appeler des services web REST d'inventaire de salles de GLO.
 * Le client gère l'obtention du token oauth2 en utilisant le bon scope du Client Credentials Grant.
 */
public class ClientServiceInventaireSallesImpl implements ClientServiceInventaireSalles {

    private static final String NOM_PARAM_REQUETE_ACCESS_TOKEN = "access_token";
    private static final String NOM_HEADER_REQUETE_API_KEY = "apikey";

    private final ClientOAuth2 clientOAuth2;
    private final CloseableHttpClient httpClient;
    private final Reference<ConfigClientServiceInventaireSalles> config;

    public ClientServiceInventaireSallesImpl(ClientOAuth2 clientOAuth2,
                                             CloseableHttpClient httpClient,
                                             Reference<ConfigClientServiceInventaireSalles> config) {

        Assert.notNull(httpClient, "httpClient is not null");
        Assert.notNull(clientOAuth2, "clientOAuth2 is not null");
        Assert.notNull(config, "config is not null");

        this.clientOAuth2 = clientOAuth2;
        this.httpClient = httpClient;
        this.config = config;
    }

    @Override
    public List<SalleGlo> obtenirInventaire() {
        ConfigClientServiceInventaireSalles configClient = config.get();
        JetonOAuth jetonOAuth = clientOAuth2.authentifierClient(configClient.scope);
        String uri = UriBuilder.fromUri(configClient.urlServeur)
                .path(configClient.cheminInventaireSalles)
                .build()
                .toString();

        HttpUriRequest request = RequestBuilder.get()
                .setUri(uri)
                .setHeader(NOM_HEADER_REQUETE_API_KEY, configClient.idClient)
                .addParameter(NOM_PARAM_REQUETE_ACCESS_TOKEN, jetonOAuth.getJetonAcces())
                .build();

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);

            if (response.getStatusLine().getStatusCode() >= 300) {
                throw new RuntimeException("Erreur lors de l'appel au service d'inventaire de GLO" + response.getStatusLine().toString() + json);
            }

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            List<SalleGlo> salles = gson.fromJson(json, new TypeToken<List<SalleGlo>>() {}.getType());

            return salles;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public String obtenirInventaireFormatJson() {
        ConfigClientServiceInventaireSalles configClient = config.get();
        JetonOAuth jetonOAuth = clientOAuth2.authentifierClient(configClient.scope);
        String uri = UriBuilder.fromUri(configClient.urlServeur)
                .path(configClient.cheminInventaireSalles)
                .build()
                .toString();

        HttpUriRequest request = RequestBuilder.get()
                .setUri(uri)
                .setHeader(NOM_HEADER_REQUETE_API_KEY, configClient.idClient)
                .addParameter(NOM_PARAM_REQUETE_ACCESS_TOKEN, jetonOAuth.getJetonAcces())
                .build();

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}

