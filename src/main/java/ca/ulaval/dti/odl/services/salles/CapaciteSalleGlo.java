package ca.ulaval.dti.odl.services.salles;


import java.io.Serializable;


/**
 * API de GLO:
 * Représente une capacité particulière de salle, en fonction de configurations différentes de celles qui se trouve au niveau de la salle.
 *
 * <h4>Implémentation</h4>
 * La capacité de salle correspond à l'élément "Salle - Configurations de salles pour événements" du MRL.
 * La capacité de salle provient de l'objet EventRoomConfigurationEmbedded de l'API Enterprise.
 */

public class CapaciteSalleGlo implements Serializable {

    /**
     * Capacité de salle pour une configuration de type "Examen"
     */
    public static final String TYPE_EXAMEN = "Examen";

    /**
     * Capacité de salle pour une configuration d'un type autre qu'examen
     */
    public static final String TYPE_AUTRE = "Autre";

    /**
     * Type de la capacité
     */
    private final String type;

    /**
     * Identifiant de la capacité (ex: Examen avec notes, Examen sans notes, Debout, Théatre, ...)
     */
    private final String nom;

    /**
     * Capacité définie dans la configuration
     */
    private final Integer capacite;

    public CapaciteSalleGlo(String type, String nom, Integer capacite) {
        this.type = type;
        this.nom = nom;
        this.capacite = capacite;
    }

    public String getType() {
        return type;
    }

    public String getNom() {
        return nom;
    }

    public Integer getCapacite() {
        return capacite;
    }
}
