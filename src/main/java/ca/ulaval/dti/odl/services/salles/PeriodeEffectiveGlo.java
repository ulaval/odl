package ca.ulaval.dti.odl.services.salles;

import java.io.Serializable;
import java.util.Date;

/**
 * API de GLO:
 * Représente une période effective au sens du module de réservation des locaux (MRL).
 *
 * Les éléments auxquels s'appliquent la période effective sont considérés comme inexistant par les APIs du produit avant et après les dates
 * mentionnées dans la période.
 *
 * <h4>Implémentation: </h4>
 * La période effective correspond à la zone "Entrée en vigueur du X au Y" visible dans les formulaires du MRL.
 * La période effective provient des attributs EffectiveStart et EffectiveEnd des objets de l'API Enterprise.
 *
 */

public class PeriodeEffectiveGlo implements Serializable {

    /**
     * Date effective de début (du X).
     *
     * Optionnel, la période est considérée effective de l'infini à la date de fin (ou l'infini si cette dernière est null).
     */
    private final Date dateDebut;

    /**
     * Date effective de fin (au Y).
     *
     * Optionnel, la période est considérée effective de la date de début  (ou l'infini si cette dernière est null) à l'infini.
     */
    private final Date dateFin;

    public PeriodeEffectiveGlo(Date dateDebut, Date dateFin) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }
}

