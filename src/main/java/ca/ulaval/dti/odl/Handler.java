package ca.ulaval.dti.odl;

import ca.ulaval.dti.odl.config.ConfigGloProperties;
import ca.ulaval.dti.odl.config.ConfigOAuthProperties;
import ca.ulaval.dti.odl.utils.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Component
public class Handler {

    @Autowired
    private ConfigGloProperties configGloProperties;

    @Autowired
    private ConfigOAuthProperties configOAuthProperties;

    public Mono<ServerResponse> hello(ServerRequest request) {
        String name = request.queryParam("capacite").isPresent() ? request.queryParam("capacite").get() : "";
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .body(BodyInserters.fromValue("Hello, Spring!" + name));
    }

    public Mono<ServerResponse> liste(ServerRequest request) {
        int capaciteMinimale = request.queryParam("capacite").isPresent() ? ParamUtils.obtenirCapaciteValidee(request.queryParam("capacite").get()) : 0;
        System.out.println("CAPACITÉ: " + capaciteMinimale);
        OdlWebClient odlwc = new OdlWebClient(configGloProperties, configOAuthProperties);

        try {
            return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                    .body(BodyInserters.fromValue(odlwc.obtenirInventaire(capaciteMinimale)));
        } catch (IOException e) {
            return null;
        }
    }
}