package ca.ulaval.dti.odl;

import ca.ulaval.dti.odl.config.*;
import ca.ulaval.dti.odl.services.ClientServiceInventaireSalles;
import ca.ulaval.dti.odl.services.ClientServiceInventaireSallesImpl;
import ca.ulaval.dti.odl.utils.Reference;
import ca.ulaval.dti.odl.utils.ReferenceSimple;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;

import java.io.IOException;

public class OdlWebClient {

    private ConfigGloProperties configGloProperties;

    private ConfigOAuthProperties configOAuthProperties;

    public OdlWebClient(ConfigGloProperties configGloProperties, ConfigOAuthProperties configOAuthProperties) {

        this.configGloProperties = configGloProperties;
        this.configOAuthProperties = configOAuthProperties;
    }

    public String obtenirInventaire(int capaciteMinimale) throws IOException {

        CloseableHttpClient HTTP_CLIENT = HttpClients.createDefault();
        CloseableHttpAsyncClient HTTP_ASYNC_CLIENT = HttpAsyncClients.createDefault();
        ConfigClientOAuth CONFIG_OAUTH = ConfigClientOAuth.configClientOAuthOdl(
                configOAuthProperties.getServeurUrl(),
                configOAuthProperties.getClientId(),
                configOAuthProperties.getClientSecret(),
                "scope_test",
                null);
        Reference<ConfigClientServiceInventaireSalles> CONFIG_SALLES = new ReferenceSimple<>(new ConfigClientServiceInventaireSalles(
                configGloProperties.getUrl(),
                configGloProperties.getCheminInventaireSalles(),
                configGloProperties.getScope(),
                configOAuthProperties.getClientId()));

        HTTP_ASYNC_CLIENT.start();

        ClientOAuth2Http clientOAuth2 = new ClientOAuth2Http(HTTP_CLIENT, HTTP_ASYNC_CLIENT, CONFIG_OAUTH);
        ClientServiceInventaireSalles client = new ClientServiceInventaireSallesImpl(clientOAuth2, HTTP_CLIENT, CONFIG_SALLES);

        //List<SalleGlo> listeSalles = client.obtenirInventaire();

        String inventaireSallesJson = client.obtenirInventaireFormatJson();

        HTTP_ASYNC_CLIENT.close();
        return inventaireSallesJson;
    }
}
