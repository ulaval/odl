package ca.ulaval.dti.odl.config;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.concurrent.FutureCallback;

/**
 * Service permettant d'implémenter la partie cliente du protocole OAuth 2.0.
 *
 */
public interface ClientOAuth2 {

    /**
     * Génère un URL pour rediriger l'utilisateur afin qu'il puisse s'authentifier.
     *
     * @param requete requete http
     * @param state Le state OAuth 2.0
     * @return L'URL
     */
    String getUrlAutorisationParCode(HttpServletRequest requete, String state);

    /**
     * Échange un code OAuth 2.0 obtenu après avoir rediriger l'utilisateur vers le serveur d'autorisation.
     *
     * @param requete requete http
     * @param code Le code OAuth 2.0 retourné par le serveur OAuth.
     * @return L'access token réflétant l'utilisateur.
     */
    JetonOAuth echangerCodePourJeton(HttpServletRequest requete, String code);

    /**
     * Permet d'authentifier un utilisateur à l'aide du 'resource owner password credentials grant'.
     *
     * @param identifiant Le code d'accès de l'utilisateur
     * @param motPasse Le mot de passe
     * @param scope Le scope OAuth 2.0
     * @return Le token
     */
    JetonOAuth authentifier(String identifiant, String motPasse, String scope);

    /**
     * Permet d'authentifier à l'aide du 'client credentials grant'.
     *
     * @param scope Le scope OAuth 2.0
     *
     * @return Le token
     */
    JetonOAuth authentifierClient(String scope);

    /**
     * Permet de rafraîchir un jeton sur le point d'expirer en asynchrone.
     *
     * Le jeton reçu doit contenir un jeton de rafraichissement.
     * Doit aussi contenir un scope si la configuration du client le necessite.
     *
     * @param jetonOAuth le jeton à rafraichir.
     * @return le nouveau jeton
     */
    JetonOAuth rafraichirJeton(JetonOAuth jetonOAuth);

    /**
     * Permet de rafraîchir un jeton sur le point d'expirer en asynchrone.
     *
     * Le jeton reçu doit contenir un jeton de rafraichissement.
     * Doit aussi contenir un scope si la configuration du client le necessite.
     *
     * @param jetonOAuth le jeton à rafraichir.
     * @param callback Le callback à invoquer de manière async lorsque complété
     */
    void rafraichirJetonAsync(JetonOAuth jetonOAuth, FutureCallback<JetonOAuth> callback);

    JetonOAuth validerJeton(String jeton) throws RuntimeException;

    /**
     * Retourne l'url permettant à l'utilisateur de se déconnecté.
     *
     * Note : ce n'est pas tous les clients qui supporte la redirection après la déconnexion.
     *
     * @param urlRedirection l'url vers laquelle rediriger l'utilisateur après la déconnexion.
     * @return l'url de déconnexion
     */
    String getUrlDeconnexion(String urlRedirection);

    /**
     * @return L'identifiant de ce client
     */
    String getIdClient();

    /**
     * @return L'url à invoquer pour prolonger la session SSO.
     */
    Optional<String> getUrlRefreshSso();
}
