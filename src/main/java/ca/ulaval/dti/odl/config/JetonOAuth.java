package ca.ulaval.dti.odl.config;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateUtils;

/**
 * {@code JetonOAuth} permet d'encapsuler les informations associées à un jeton OAuth, que ce soit un code d'autorisation ou un jeton d'accès.
 * Un jeton OAuth est accordé à un {@link #identifiantClient client} pour qu'il puisse impersonifié
 * un {@link #codeUtilisateur utilisateur spécifique}.
 *
 * <p>IMPORTANT: Un jeton OAuth est toujours temporaire et sa validité doit être vérifié avec la méthode {@link #isValide()}.</p>
 *
 * @author ollaf1
 */
public final class JetonOAuth implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum TypeCompte {
        /**
         * Un jeton émis par un système pour un utilisateur précis.
         */
        UTILISATEUR,

        /**
         * Un jeton émis par "client credential grant" qui représente un système.
         */
        APPLICATION,

        /**
         * Un jeton émis par un système pour un utilisateur brio.
         */
        UTILISATEUR_BRIO,

        /**
         * ?
         */
        INCONNU
    }

    /** Énumération des types de jeton. */
    public enum TypeJeton {
        /**
         * Représente un code d'autorisation généré durant le processus d'authentification.
         * Ce code ne doit pas donner accès à rien mais permet de l'échanger pour un vrai jeton d'accès.
         */
        CODE_AUTORISATION,

        /**
         * Représente un jeton d'accès standard OAuth 2.0. Ce jeton peut être associé à un utilisateur ainsi
         * qu'à une application cliente.
         */
        JETON_ACCES
    }

    /**
     * Énumération des types de jeton d'accès de oauth2.
     * Pour le moment le type "Bearer" est le seul type supporté, mais l'impléementation est fait pour prendre
     * en compte d'autres types au cas où on le voudrait dans l'avenir.
     * @author Souleymane NDIAYE
     *
     */
    public enum TypeJetonAcces {
        /**
         * Représente le type "BEARER" du jeton d'accès standard OAuth 2.0.
         */
        BEARER("Bearer"),

        /**
         * Représente le type "MAC" du jeton d'accès standard OAuth 2.0.
         */
        MAC("Mac");

        /** La valeur associée au type du jeton d'accès */
        private String valeur;

        /**
         * @param valeur La valeur associée au type du jeton d'accès
         */
        private TypeJetonAcces(String valeur) {
            this.valeur = valeur;
        }

        public String getValeur() {
            return valeur;
        }
    }

    /**
     * Crée un builder pour créer un accès OAuth valide.
     *
     * @return un nouveau builder pour créer un accès OAuth valide
     */
    public static JetonOAuth.Builder builder() {
        return new JetonOAuth.Builder();
    }

    private final String idUtilisateur;
    private final String codeUtilisateur;
    private final String identifiantClient;
    private final TypeJeton typeJeton;
    private final String jeton;
    private final String jetonRafraichissement;
    private final Date dateExpiration;
    private final String urlOrigine;
    private final TypeCompte typeCompte;
    private final String scope;
    private final TypeJetonAcces typeJetonAcces;

    /**
     * Constructeur.
     *
     * @param builder le builder
     */
    private JetonOAuth(Builder builder) {
        this.idUtilisateur = builder.idUtilisateur;
        this.codeUtilisateur = builder.codeUtilisateur;
        this.identifiantClient = builder.identifiantClient;
        this.typeJeton = builder.typeJeton;
        this.jeton = builder.jeton;
        this.jetonRafraichissement = builder.jetonRafraichissement;
        this.dateExpiration = builder.dateExpiration;
        this.urlOrigine = builder.urlOrigine;
        this.typeCompte = builder.typeCompte == null ? TypeCompte.INCONNU : builder.typeCompte;
        this.scope = builder.scope;
        this.typeJetonAcces = builder.typeJetonAcces;
    }

    /**
     * Retourne l'id de l'utilisateur. Peut être null si celui-ci n'est pas fournit par le service oauth2 qui a émit le jeton.
     *
     * @return l'id de l'utilisateur ou null si celui-ci n'était pas fournit.
     */
    public String getIdUtilisateur() {
        return idUtilisateur;
    }

    /**
     * @return code de l'utilisateur impersonifié
     */
    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    /**
     * @return l'identifiant du client qui s'est vu accordé l'accès
     */
    public String getIdentifiantClient() {
        return identifiantClient;
    }

    /**
     * @return le code d'autorisation OAuth
     */
    public String getCodeAutorisation() {
        if (typeJeton == TypeJeton.CODE_AUTORISATION) {
            return jeton;
        }

        return null;
    }

    /**
     * @return le jeton d'accès OAuth
     */
    public String getJetonAcces() {
        if (typeJeton == TypeJeton.JETON_ACCES) {
            return jeton;
        }

        return null;
    }

    /**
     * @return le type de jeton d'accès OAuth.
     */
    public String getTypeJetonAcces() {
        if (typeJeton == TypeJeton.JETON_ACCES) {
            // MPO s'attend à recevoir le type du jeton d'accès oauth2 sous la forme "Bearer"
            // Pour le moment c'est le seul type supporté, mais l'implémentation est fait pour prendre
            // en compte d'autres types au cas où on le voudrait dans l'avenir.
            return typeJetonAcces.getValeur();
        }

        return null;
    }

    /**
     * @return Le refresh_token OAuth 2.0
     */
    public String getJetonRafraichissement() {
        return jetonRafraichissement;
    }

    /**
     * Indique si ce jeton peut être rafraîchi.
     *
     * @return {@code true} si ce jeton peut être rafraîchi, sinon {@code false}
     */
    public boolean isJetonRafraichissable() {
        return StringUtils.isNotBlank(jetonRafraichissement);
    }

    /**
     * @return la date d'expiration
     */
    public Date getDateExpiration() {
        return dateExpiration;
    }

    /**
     * Dans le contexte d'un utilisateur qui s'authentifie à l'aide d'un code, l'url d'origine
     * représente l'URL qui a déclenché l'authentification et où on doit retourner après.
     * @return l'url d'origine
     */
    public String getUrlOrigine() {
        return urlOrigine;
    }

    public TypeCompte getTypeCompte() {
        return typeCompte;
    }

    /**
     * @return Le scope OAuth 2.0 pour lequel le jeton a été émis
     */
    public String getScope() {
        return scope;
    }

    /**
     * @return {@code true} is cet accès est valide, sinon {@code false}
     */
    public boolean isValide() {
        return dateExpiration.getTime() > System.currentTimeMillis();
    }

    /**
     * Valide le jeton.
     *
     * @throws RuntimeException si c'est un code d'autorisation invalide
     * @throws RuntimeException si c'est un jeton d'accès invalide
     */
    public void valider() throws RuntimeException {
        if (! isValide()) {
            lancerExceptionJetonOAuthInvalide("Jeton expiré.");
        }
    }

    /**
     * Valide que ce jeton est valide pour le client donné.
     *
     * @param identifiantClient l'identifiant du client, obligatoire
     * @throws RuntimeException si c'est un code d'autorisation invalide
     * @throws RuntimeException si c'est un jeton d'accès invalide
     */
    public void validerPourClient(String identifiantClient) {
        valider();

        if (clientDifferent(identifiantClient)) {
            lancerExceptionJetonOAuthInvalide("Client différent.");
        }
    }

    /**
     * Calcul la durée restant en milli avant l'expiration du jeton.
     *
     * @return Si la date est expiré retourne 0, sinon la différence entre la date d'expiration et la date systeme en millisecondes
     */
    public long getDureeAvantExpiration() {
        return Math.max(0L, dateExpiration.getTime() - System.currentTimeMillis());
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + Objects.hashCode(typeJeton);
        hash = 31 * hash + Objects.hashCode(jeton);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof JetonOAuth) {
            JetonOAuth that = (JetonOAuth) obj;
            return Objects.equals(this.typeJeton, that.typeJeton) && Objects.equals(this.jeton, that.jeton);
        }

        return false;
    }

    @Override
    public String toString() {
        return "JetonOAuth [codeUtilisateur="
                + codeUtilisateur
                + ", identifiantClient="
                + identifiantClient
                + ", typeJeton="
                + typeJeton
                + ", jeton="
                + jeton
                + ", dateExpiration="
                + dateExpiration
                + ", typeJetonAcces="
                + typeJetonAcces
                + "]";
    }

    private void lancerExceptionJetonOAuthInvalide(String raison) throws RuntimeException {
        String detail = "Raison: " + raison + ". Jeton: " + toString() + ".";

        if (typeJeton == TypeJeton.CODE_AUTORISATION) {
            throw new RuntimeException("Code d'autorisation invalide. " + detail);
        }

        throw new RuntimeException("Jeton d'accès invalide. " + detail);
    }

    private boolean clientDifferent(String identifiantClient) {
        return ObjectUtils.notEqual(this.identifiantClient, identifiantClient);
    }

    /**
     * Builder pattern.
     */
    public static class Builder {

        private String idUtilisateur;
        private String codeUtilisateur;
        private String identifiantClient;
        private TypeJeton typeJeton;
        private String jeton;
        private String jetonRafraichissement;
        private Date dateExpiration;
        private String urlOrigine;
        private TypeCompte typeCompte;
        private String scope;
        private TypeJetonAcces typeJetonAcces = TypeJetonAcces.BEARER;

        public Builder() {

        }

        public Builder(JetonOAuth jeton) {
            this.idUtilisateur = jeton.idUtilisateur;
            this.codeUtilisateur = jeton.codeUtilisateur;
            this.identifiantClient = jeton.identifiantClient;
            this.typeJeton = jeton.typeJeton;
            this.jeton = jeton.jeton;
            this.jetonRafraichissement = jeton.jetonRafraichissement;
            this.dateExpiration = jeton.dateExpiration;
            this.urlOrigine = jeton.urlOrigine;
            this.typeCompte = jeton.typeCompte;
            this.scope = jeton.scope;
            this.typeJetonAcces = jeton.typeJetonAcces;
        }

        public Builder idUtilisateur(String idUtilisateur) {
            this.idUtilisateur = idUtilisateur;
            return this;
        }

        public Builder codeUtilisateur(String codeUtilisateur) {
            this.codeUtilisateur = codeUtilisateur;
            return this;
        }

        public Builder identifiantClient(String identifiantClient) {
            this.identifiantClient = identifiantClient;
            return this;
        }

        public Builder codeAutorisation(String codeAutorisation) {
            this.typeJeton = TypeJeton.CODE_AUTORISATION;
            this.jeton = codeAutorisation;
            return this;
        }

        public Builder jetonAcces(String jetonAcces) {
            this.typeJeton = TypeJeton.JETON_ACCES;
            this.jeton = jetonAcces;
            return this;
        }

        public Builder jetonRafraichissement(String jetonRafraichissement) {
            this.jetonRafraichissement = jetonRafraichissement;
            return this;
        }

        public Builder dateExpiration(Date dateExpiration) {
            this.dateExpiration = dateExpiration;
            return this;
        }

        public Builder urlOrigine(String urlOrigine) {
            this.urlOrigine = urlOrigine;
            return this;
        }

        public Builder typeCompte(TypeCompte typeCompte) {
            this.typeCompte = typeCompte;
            return this;
        }

        public BuilderDateExpiration dateExpirationDans(int nombre) {
            return new BuilderDateExpiration(nombre);
        }

        public Builder scope(String scope) {
            this.scope = scope;
            return this;
        }

        public Builder typeJetonAcces(TypeJetonAcces typeJetonAcces) {
            this.typeJetonAcces = typeJetonAcces;
            return this;
        }

        public JetonOAuth build() {
            Validate.notEmpty(codeUtilisateur);
            Validate.notEmpty(identifiantClient);
            Validate.notNull(typeJeton);
            Validate.notEmpty(jeton);
            Validate.notNull(dateExpiration);
            Validate.notNull(typeJetonAcces);

            return new JetonOAuth(this);
        }

        public class BuilderDateExpiration {

            private final int nombre;

            public BuilderDateExpiration(int nombre) {
                this.nombre = nombre;
            }

            public Builder millisecondes() {
                Builder.this.dateExpiration = DateUtils.addMilliseconds(new Date(), nombre);
                return Builder.this;
            }

            public Builder secondes() {
                Builder.this.dateExpiration = DateUtils.addSeconds(new Date(), nombre);
                return Builder.this;
            }

            public Builder minutes() {
                Builder.this.dateExpiration = DateUtils.addMinutes(new Date(), nombre);
                return Builder.this;
            }

            public Builder heures() {
                Builder.this.dateExpiration = DateUtils.addHours(new Date(), nombre);
                return Builder.this;
            }
        }
    }
}
