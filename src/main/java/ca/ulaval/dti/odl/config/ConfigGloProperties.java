package ca.ulaval.dti.odl.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "odl.apiglo")
public class ConfigGloProperties {

    private String cheminInventaireSalles;
    private String scope;
    private String url;

    public String getCheminInventaireSalles() {
        return cheminInventaireSalles;
    }

    public void setCheminInventaireSalles(String cheminInventaireSalles) {
        this.cheminInventaireSalles = cheminInventaireSalles;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
