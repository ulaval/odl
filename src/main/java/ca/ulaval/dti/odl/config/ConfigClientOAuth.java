package ca.ulaval.dti.odl.config;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Classe servant à configurer le client OAuth 2.0.
 *
 */
public class ConfigClientOAuth implements Serializable, Cloneable {

    private static final String ODL_CHEMIN_AUTHENTIFICATION = "/ul/auth/oauth/v2/authorize";
    private static final String ODL_CHEMIN_TOKEN = "/ul/auth/oauth/v2/token";
    private static final String ODL_CHEMIN_VALIDATION = "/ul/auth/oauth/v2/token/introspection";
    private static final String ODL_CHEMIN_FERMETURE_SESSION = "/ul/auth/oauth/v2/logoutsso";
    private static final String ODL_CHEMIN_REFRESH_SSO = "/ul/auth/oauth/v2/refreshsession";

    private static final String ODL_ATTRIBUT_REPONSE_CODE_UTILISATEUR = "resource_owner";

    private static final JetonOAuth.TypeCompte TYPE_COMPTE_DEFAUT_ODL = JetonOAuth.TypeCompte.INCONNU;

    private static final String CHEMIN_RETOUR_UTILISATEUR = "/services/oauth2/retour/";

    /**
     * L'url de base du serveur.
     */
    public final String urlServeur;

    /**
     * URL où rediriger l'utilisateur afin de déclencher une fermeture de session SSO.
     */
    public final String urlFermetureSession;

    /**
     * Url de base pour authentifier un utilisateur.
     */
    public final String urlAuthentification;

    /**
     * Uurl de base pour obtenir un token.
     */
    public final String urlToken;

    /**
     * Url de base pour la validation d'un jeton.
     */
    public final String urlValidation;

    /**
     * Url pour le refresh de la session SSO.
     */
    public final Optional<String> urlRefreshSso;

    /**
     * Notre clientId pour invoquer le serveur OAuth.
     */
    public final String idClient;

    /**
     * Notre mot de passe pour invoquer le serveur OAuth.
     */
    public final String secretClient;

    /**
     * Le scope a utilisé lors de l'authentification de l'utilisateur
     */
    public final String scope;

    /**
     * Indique si le scope est obligatoire (dans l'implémentation de brio, le scope n'est pas utilisé).
     */
    public final boolean scopeObligatoire;

    /**
     * La partie à ajouter à l'url de base pour rediriger l'utilisateur après qu'il se soit authentifié.
     */
    public final String cheminRetourUtilisateur;

    /**
     * Le nom de l'attribut représentant le code utilisateur dans la réponse de la requête token.
     */
    public final String attributReponseCodeUtilisateur;

    /**
     * Indique si le clientId/secret doivent être url encoded dans l'entête d'authentification basic.
     */
    public final boolean authentificationBasicUrlEncoded;

    /**
     * Le charset à utiliser pour l'authentification basic.
     */
    public final String authentificationBasicCharset;

    /**
     * Indique si l'url de déconnexion supporte la redirection.
     */
    public final boolean supporteRedirectionApresDeconnexion;

    /**
     * Le type de compte à utiliser lorsque le type de compte n'est pas fournit par le service oauth2.
     */
    public final JetonOAuth.TypeCompte typeCompteDefaut;

    /**
     * Valeur de l'étiquette qui est ajouté aux des requêtes faites par le {@link ClientOAuth2}.
     * Si {@code null} il n'y a pas d'étiquettes ajoutés aux requêtes.
     */
    public final String etiquetteRequete;

    public static ConfigClientOAuth configClientOAuthOdl(String urlServeur, String idClient, String secretClient, String scope, String etiquetteRequete) {
        return new ConfigClientOAuth(
                urlServeur,
                urlServeur + ODL_CHEMIN_FERMETURE_SESSION,
                urlServeur + ODL_CHEMIN_AUTHENTIFICATION,
                urlServeur + ODL_CHEMIN_TOKEN,
                urlServeur + ODL_CHEMIN_VALIDATION,
                urlServeur + ODL_CHEMIN_REFRESH_SSO,
                idClient,
                secretClient,
                scope,
                true,
                CHEMIN_RETOUR_UTILISATEUR,
                ODL_ATTRIBUT_REPONSE_CODE_UTILISATEUR,
                false,
                StandardCharsets.ISO_8859_1.name(),
                true,
                TYPE_COMPTE_DEFAUT_ODL,
                etiquetteRequete);
    }

    public ConfigClientOAuth(String urlServeur,
                             String urlFermetureSession,
                             String urlAuthentification,
                             String urlToken,
                             String urlValidation,
                             String urlRefreshSso,
                             String idClient,
                             String secretClient,
                             String scope,
                             boolean scopeObligatoire,
                             String cheminRetourUtilisateur,
                             String attributReponseCodeUtilisateur,
                             boolean authentificationBasicUrlEncoded,
                             String authentificationBasicCharset,
                             boolean supporteRedirectionApresDeconnexion,
                             JetonOAuth.TypeCompte typeCompteDefaut,
                             String etiquetteRequete) {

        Validate.isTrue(!scopeObligatoire || StringUtils.isNotBlank(scope), "Le scope doit être fournit si celui-ci est obligatoire");

        this.urlServeur = urlServeur;
        this.urlFermetureSession = urlFermetureSession;
        this.urlAuthentification = urlAuthentification;
        this.urlToken = urlToken;
        this.urlValidation = urlValidation;
        this.urlRefreshSso = Optional.ofNullable(urlRefreshSso);
        this.idClient = idClient;
        this.secretClient = secretClient;
        this.scope = scope;
        this.scopeObligatoire = scopeObligatoire;
        this.cheminRetourUtilisateur = cheminRetourUtilisateur;
        this.attributReponseCodeUtilisateur = attributReponseCodeUtilisateur;
        this.authentificationBasicUrlEncoded = authentificationBasicUrlEncoded;
        this.authentificationBasicCharset = authentificationBasicCharset;
        this.supporteRedirectionApresDeconnexion = supporteRedirectionApresDeconnexion;
        this.typeCompteDefaut = Optional.ofNullable(typeCompteDefaut).orElse(JetonOAuth.TypeCompte.INCONNU);
        this.etiquetteRequete = etiquetteRequete;
    }


    @Override
    public String toString() {
        return urlValidation + ':' + idClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConfigClientOAuth that = (ConfigClientOAuth) o;
        return scopeObligatoire == that.scopeObligatoire &&
                authentificationBasicUrlEncoded == that.authentificationBasicUrlEncoded &&
                supporteRedirectionApresDeconnexion == that.supporteRedirectionApresDeconnexion &&
                Objects.equals(urlServeur, that.urlServeur) &&
                Objects.equals(urlFermetureSession, that.urlFermetureSession) &&
                Objects.equals(urlAuthentification, that.urlAuthentification) &&
                Objects.equals(urlToken, that.urlToken) &&
                Objects.equals(urlValidation, that.urlValidation) &&
                Objects.equals(idClient, that.idClient) &&
                Objects.equals(secretClient, that.secretClient) &&
                Objects.equals(scope, that.scope) &&
                Objects.equals(cheminRetourUtilisateur, that.cheminRetourUtilisateur) &&
                Objects.equals(attributReponseCodeUtilisateur, that.attributReponseCodeUtilisateur) &&
                Objects.equals(authentificationBasicCharset, that.authentificationBasicCharset) &&
                Objects.equals(typeCompteDefaut, that.typeCompteDefaut);
    }

    @Override
    public int hashCode() {

        return Objects.hash(
                urlServeur,
                urlFermetureSession,
                urlAuthentification,
                urlToken,
                urlValidation,
                idClient,
                secretClient,
                scope,
                scopeObligatoire,
                cheminRetourUtilisateur,
                attributReponseCodeUtilisateur,
                authentificationBasicUrlEncoded,
                authentificationBasicCharset,
                supporteRedirectionApresDeconnexion,
                typeCompteDefaut);
    }

    @Override
    public ConfigClientOAuth clone() {
        try {
            return (ConfigClientOAuth) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ConfigClientOAuthBuilder toBuilder() {
        return new ConfigClientOAuthBuilder(this);
    }

    public static ConfigClientOAuthBuilder builder() {
        return new ConfigClientOAuthBuilder();
    }

    public static final class ConfigClientOAuthBuilder {

        private String urlServeur;
        private String urlFermetureSession;
        private String urlAuthentification;
        private String urlToken;
        private String urlValidation;
        private String urlRefreshSso;
        private String idClient;
        private String secretClient;
        private String scope;
        private boolean scopeObligatoire;
        private String cheminRetourUtilisateur;
        private String attributReponseCodeUtilisateur;
        private boolean authentificationBasicUrlEncoded;
        private String authentificationBasicCharset;
        private boolean supporteRedirectionApresDeconnexion;
        private JetonOAuth.TypeCompte typeCompteDefaut;

        private ConfigClientOAuthBuilder() {
        }

        public ConfigClientOAuthBuilder(ConfigClientOAuth configClientOAuth) {
            this.urlServeur = configClientOAuth.urlServeur;
            this.urlFermetureSession = configClientOAuth.urlFermetureSession;
            this.urlAuthentification = configClientOAuth.urlAuthentification;
            this.urlToken = configClientOAuth.urlToken;
            this.urlValidation = configClientOAuth.urlValidation;
            this.urlRefreshSso = configClientOAuth.urlRefreshSso.orElse(null);
            this.idClient = configClientOAuth.idClient;
            this.secretClient = configClientOAuth.secretClient;
            this.scope = configClientOAuth.scope;
            this.scopeObligatoire = configClientOAuth.scopeObligatoire;
            this.cheminRetourUtilisateur = configClientOAuth.cheminRetourUtilisateur;
            this.attributReponseCodeUtilisateur = configClientOAuth.attributReponseCodeUtilisateur;
            this.authentificationBasicUrlEncoded = configClientOAuth.authentificationBasicUrlEncoded;
            this.authentificationBasicCharset = configClientOAuth.authentificationBasicCharset;
            this.supporteRedirectionApresDeconnexion = configClientOAuth.supporteRedirectionApresDeconnexion;
            this.typeCompteDefaut = configClientOAuth.typeCompteDefaut;
        }

        public ConfigClientOAuthBuilder withUrlServeur(String urlServeur) {
            this.urlServeur = urlServeur;
            return this;
        }

        public ConfigClientOAuthBuilder withUrlFermetureSession(String urlFermetureSession) {
            this.urlFermetureSession = urlFermetureSession;
            return this;
        }

        public ConfigClientOAuthBuilder withUrlAuthentification(String urlAuthentification) {
            this.urlAuthentification = urlAuthentification;
            return this;
        }

        public ConfigClientOAuthBuilder withUrlToken(String urlToken) {
            this.urlToken = urlToken;
            return this;
        }

        public ConfigClientOAuthBuilder withUrlValidation(String urlValidation) {
            this.urlValidation = urlValidation;
            return this;
        }

        public ConfigClientOAuthBuilder withIdClient(String idClient) {
            this.idClient = idClient;
            return this;
        }

        public ConfigClientOAuthBuilder withSecretClient(String secretClient) {
            this.secretClient = secretClient;
            return this;
        }

        public ConfigClientOAuthBuilder withScope(String scope) {
            this.scope = scope;
            return this;
        }

        public ConfigClientOAuthBuilder withScopeObligatoire(boolean scopeObligatoire) {
            this.scopeObligatoire = scopeObligatoire;
            return this;
        }

        public ConfigClientOAuthBuilder withCheminRetourUtilisateur(String cheminRetourUtilisateur) {
            this.cheminRetourUtilisateur = cheminRetourUtilisateur;
            return this;
        }

        public ConfigClientOAuthBuilder withAttributReponseCodeUtilisateur(String attributReponseCodeUtilisateur) {
            this.attributReponseCodeUtilisateur = attributReponseCodeUtilisateur;
            return this;
        }

        public ConfigClientOAuthBuilder withAuthentificationBasicUrlEncoded(boolean authentificationBasicUrlEncoded) {
            this.authentificationBasicUrlEncoded = authentificationBasicUrlEncoded;
            return this;
        }

        public ConfigClientOAuthBuilder withAuthentificationBasicCharset(String authentificationBasicCharset) {
            this.authentificationBasicCharset = authentificationBasicCharset;
            return this;
        }

        public ConfigClientOAuthBuilder withSupporteRedirectionApresDeconnexion(boolean supporteRedirectionApresDeconnexion) {
            this.supporteRedirectionApresDeconnexion = supporteRedirectionApresDeconnexion;
            return this;
        }

        public ConfigClientOAuthBuilder withTypeCompteDefaut(JetonOAuth.TypeCompte typeCompteDefaut) {
            this.typeCompteDefaut = typeCompteDefaut;
            return this;
        }

        public ConfigClientOAuth build() {
            return new ConfigClientOAuth(
                    urlServeur,
                    urlFermetureSession,
                    urlAuthentification,
                    urlToken,
                    urlValidation,
                    urlRefreshSso,
                    idClient,
                    secretClient,
                    scope,
                    scopeObligatoire,
                    cheminRetourUtilisateur,
                    attributReponseCodeUtilisateur,
                    authentificationBasicUrlEncoded,
                    authentificationBasicCharset,
                    supporteRedirectionApresDeconnexion,
                    typeCompteDefaut,
                    null);
        }
    }
}
