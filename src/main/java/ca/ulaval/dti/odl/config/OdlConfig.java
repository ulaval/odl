package ca.ulaval.dti.odl.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"ca.ulaval.dti.odl.services"})
@EnableConfigurationProperties(
        value = {ConfigOAuthProperties.class, ConfigGloProperties.class})
public class OdlConfig {

}