package ca.ulaval.dti.odl.config;

import ca.ulaval.dti.odl.config.JetonOAuth.TypeCompte;
import ca.ulaval.dti.odl.config.JetonOAuth.TypeJetonAcces;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.util.EntityUtils;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

public class ClientOAuth2Http implements ClientOAuth2 {

    //Header utilisés dans les requêtes envoyées au serveur d'autorisation
    private static final String NOM_HEADER_AUTHORIZATION = "Authorization";

    //Paramètres utilisés dans les requêtes envoyées au serveur d'autorisation
    private static final String NOM_PARAM_REQUETE_RESPONSE_TYPE = "response_type";
    private static final String NOM_PARAM_REQUETE_CLIENT_ID = "client_id";
    private static final String NOM_PARAM_REQUETE_CLIENT_SECRET = "client_secret";
    private static final String NOM_PARAM_REQUETE_API_KEY = "apikey";
    private static final String NOM_PARAM_REQUETE_REDIREDCT_URI = "redirect_uri";
    private static final String NOM_PARAM_REQUETE_SCOPE = "scope";
    private static final String NOM_PARAM_REQUETE_GRANT_TYPE = "grant_type";
    private static final String NOM_PARAM_REQUETE_CODE = "code";
    private static final String NOM_PARAM_REQUETE_USERNAME = "username";
    private static final String NOM_PARAM_REQUETE_PASSWORD = "password";
    private static final String NOM_PARAM_REQUETE_STATE = "state";
    private static final String NOM_PARAM_REQUETE_REFRESH_TOKEN = "refresh_token";
    private static final String NOM_PARAM_REQUETE_LOGOUT_CONTINUE_URL = "urlRetourLogout";
    private static final String NOM_PARAM_REQUETE_VALIDATION_TOKEN = "token";
    private static final String NOM_PARAM_REQUETE_ETIQUETTE = "tag";

    //Valeurs des paramètres utilisés dans les requêtes
    private static final String VALEUR_PARAM_REQUETE_GRANT_TYPE_AUTH_CODE = "authorization_code";
    private static final String VALEUR_PARAM_REQUETE_GRANT_TYPE_PASSWORD = "password";
    private static final String VALEUR_PARAM_REQUETE_GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
    private static final String VALEUR_PARAM_REQUETE_GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    private static final String VALEUR_PARAM_REQUETE_RESPONSE_TYPE = "code";

    //Attributs des reponses Json du serveur d'autorisation
    private static final String ATTRIBUT_REPONSE_TOKEN = "access_token";
    private static final String ATTRIBUT_REPONSE_EXPIRES_IN = "expires_in";
    private static final String ATTRIBUT_REPONSE_EXPIRATION = "exp";
    private static final String ATTRIBUT_REPONSE_REFRESH_TOKEN = "refresh_token";
    private static final String ATTRIBUT_REPONSE_SCOPE = "scope";
    private static final String ATTRIBUT_REPONSE_RESOURCE_OWNER_TYPE = "resource_owner_type";
    private static final String ATTRIBUT_REPONSE_TOKEN_TYPE = "token_type";
    private static final String ATTRIBUT_REPONSE_CLIENT_ID = "client_id";
    private static final String ATTRIBUT_REPONSE_VALIDATION_RESULT = "active";
    private static final String ATTRIBUT_REPONSE_USER_NAME = "username";
    private static final String ATTRIBUT_REPONSE_USER_ID = "userid";

    private static final String VALEUR_RESOURCE_OWNER_TYPE_UTILISATEUR = "user";
    private static final String VALEUR_RESOURCE_OWNER_TYPE_APPLICATION = "app";

    private static final String VALEUR_TYPE_JETON_ACCES_BEARER = "Bearer";
    private static final String VALEUR_TYPE_JETON_ACCES_MAC = "Mac";

    //Valeur des attributs des reponses Json du serveur d'autorisation
    private static final String VALEUR_ATTRIBUT_REPONSE_VALIDATION_RESULT_VALID = "true";

    protected final CloseableHttpClient httpClient;
    protected final CloseableHttpAsyncClient httpAsyncClient;
    protected final ConfigClientOAuth config;

    public ClientOAuth2Http(CloseableHttpClient httpClient, CloseableHttpAsyncClient httpAsyncClient, ConfigClientOAuth config) {

        Assert.notNull(httpClient, "httpClient is null");
        Assert.notNull(httpAsyncClient, "httpAsyncClient is null");
        Assert.notNull(config, "config is null");

        this.httpClient = httpClient;
        this.httpAsyncClient = httpAsyncClient;
        this.config = config;
    }

    @Override
    public String getIdClient() {
        return config.idClient;
    }


    @Override
    public String getUrlAutorisationParCode(HttpServletRequest requete, String state) {
        Assert.hasText(state, "Le paramètre state est obligatoire pour éviter certaines vulnérabilités.");

        UriBuilder uriBuilder = UriBuilder.fromUri(config.urlAuthentification)
                .queryParam(NOM_PARAM_REQUETE_RESPONSE_TYPE, VALEUR_PARAM_REQUETE_RESPONSE_TYPE)
                .queryParam(NOM_PARAM_REQUETE_CLIENT_ID, config.idClient)
                .queryParam(NOM_PARAM_REQUETE_REDIREDCT_URI, batirUrlRetourUtilisateur(config, requete))
                .queryParam(NOM_PARAM_REQUETE_STATE, state);

        if (StringUtils.hasText(config.scope)) {
            uriBuilder.queryParam(NOM_PARAM_REQUETE_SCOPE, config.scope);
        }

        ajouterQueryParamEtiquetteSiNecessaire(uriBuilder);

        return uriBuilder.build().toString();
    }

    /**
     * Remplace le nom du serveur dans l'url de retour par celui provenant de la requête courante.  Permet de redirigier l'utilisateur sur le noeud avec
     * lequel il s'authentifie.
     *
     * @param c config oaut
     * @return url de retour de l'authentification oauth.
     */
    private String batirUrlRetourUtilisateur(ConfigClientOAuth c, HttpServletRequest requete) {

        // On force https pour être certain que l'échange est sécuritaire
        // et pour contourner un problème sur Openshift qui ne supporte pas
        // de base le header X-Forwarded-Proto
        String urlBase = "https://" + requete.getServerName();

        int serverPort = requete.getServerPort();
        if (serverPort != 80 && serverPort != 443) {
            urlBase += ":" + requete.getServerPort();
        }

        return UriBuilder.fromUri(urlBase)
                .path(c.cheminRetourUtilisateur)
                .build()
                .toString();
    }

    @Override
    public JetonOAuth echangerCodePourJeton(HttpServletRequest requete, String code) {

        String uri = UriBuilder.fromUri(config.urlToken)
                .build()
                .toString();

        RequestBuilder request = RequestBuilder.post()
                .setUri(uri)
                .addParameter(NOM_PARAM_REQUETE_GRANT_TYPE, VALEUR_PARAM_REQUETE_GRANT_TYPE_AUTH_CODE)
                .addParameter(NOM_PARAM_REQUETE_CODE, code)
                .addParameter(NOM_PARAM_REQUETE_REDIREDCT_URI, batirUrlRetourUtilisateur(config, requete))
                .addParameter(NOM_PARAM_REQUETE_CLIENT_ID, config.idClient)
                .addParameter(NOM_PARAM_REQUETE_CLIENT_SECRET, config.secretClient);
        ajouterHeaderEtiquetteSiNecessaire(request);

        return executerRequeteToken(request.build(), VALEUR_PARAM_REQUETE_GRANT_TYPE_AUTH_CODE);
    }

    @Override
    public JetonOAuth authentifier(String identifiant, String motPasse, String scope) {

        String uri = UriBuilder.fromUri(config.urlToken)
                .build()
                .toString();

        RequestBuilder request = RequestBuilder.post()
                .setUri(uri)
                .setHeader(NOM_HEADER_AUTHORIZATION, construireEnteteBasicAuthentification())
                .addParameter(NOM_PARAM_REQUETE_GRANT_TYPE, VALEUR_PARAM_REQUETE_GRANT_TYPE_PASSWORD)
                .addParameter(NOM_PARAM_REQUETE_USERNAME, identifiant)
                .addParameter(NOM_PARAM_REQUETE_PASSWORD, motPasse)
                .addParameter(NOM_PARAM_REQUETE_SCOPE, scope);
        ajouterHeaderEtiquetteSiNecessaire(request);

        return executerRequeteToken(request.build(), VALEUR_PARAM_REQUETE_GRANT_TYPE_PASSWORD);
    }

    @Override
    public JetonOAuth authentifierClient(String scope) {
        String uri = UriBuilder.fromUri(config.urlToken)
                .build()
                .toString();

        RequestBuilder request = RequestBuilder.post()
                .setUri(uri)
                .addParameter(NOM_PARAM_REQUETE_GRANT_TYPE, VALEUR_PARAM_REQUETE_GRANT_TYPE_CLIENT_CREDENTIALS)
                .addParameter(NOM_PARAM_REQUETE_CLIENT_ID, config.idClient)
                .addParameter(NOM_PARAM_REQUETE_CLIENT_SECRET, config.secretClient)
                .addParameter(NOM_PARAM_REQUETE_SCOPE, scope);
        ajouterHeaderEtiquetteSiNecessaire(request);

        return executerRequeteToken(request.build(), VALEUR_PARAM_REQUETE_GRANT_TYPE_CLIENT_CREDENTIALS);
    }

    @Override
    public JetonOAuth rafraichirJeton(JetonOAuth jetonActuel) {
        validerJetonPourRafraichissement(jetonActuel);
        HttpUriRequest request = preparerRequeteRafraichissementJeton(jetonActuel.getJetonRafraichissement(), jetonActuel.getScope());

        return executerRequeteToken(request, VALEUR_PARAM_REQUETE_GRANT_TYPE_REFRESH_TOKEN);
    }

    @Override
    public void rafraichirJetonAsync(JetonOAuth jetonActuel, final FutureCallback<JetonOAuth> callback) {
        validerJetonPourRafraichissement(jetonActuel);
        HttpUriRequest request = preparerRequeteRafraichissementJeton(jetonActuel.getJetonRafraichissement(), jetonActuel.getScope());

        httpAsyncClient.execute(request, new FutureCallback<HttpResponse>() {

            @Override
            public void completed(HttpResponse response) {
                JetonOAuth jeton = null;

                try {
                    HttpEntity entity = response.getEntity();

                    String reponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode < 200 || statusCode >= 300) {
                        throw new RuntimeException("Erreur lors du rafraichissement du token OAuth: " + response.getStatusLine().toString() + reponse);
                    }

                    jeton = jsonToJeton(config.idClient, reponse);
                } catch (RuntimeException e) {
                    callback.failed(e);
                    throw e;
                } catch (IOException e) {
                    callback.failed(new RuntimeException(e));
                    throw new RuntimeException(e);
                } catch (Throwable e) {
                    callback.failed(new RuntimeException(e));
                    throw e;
                }

                if (jeton != null) {
                    callback.completed(jeton);
                }
            }

            @Override
            public void cancelled() {
                callback.cancelled();
            }

            @Override
            public void failed(Exception e) {
                callback.failed(e);
            }
        });
    }

    private HttpUriRequest preparerRequeteRafraichissementJeton(String refreshToken, String scope) {
        String uri = UriBuilder.fromUri(config.urlToken)
                .build()
                .toString();

        RequestBuilder builder = RequestBuilder.post()
                .setUri(uri)
                .addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .addParameter(NOM_PARAM_REQUETE_GRANT_TYPE, VALEUR_PARAM_REQUETE_GRANT_TYPE_REFRESH_TOKEN)
                .addParameter(NOM_PARAM_REQUETE_SCOPE, scope)
                .addParameter(NOM_PARAM_REQUETE_CLIENT_ID, config.idClient)
                .addParameter(NOM_PARAM_REQUETE_CLIENT_SECRET, config.secretClient)
                .addParameter(NOM_PARAM_REQUETE_REFRESH_TOKEN, refreshToken);
        ajouterHeaderEtiquetteSiNecessaire(builder);

        return builder.build();
    }

    private void validerJetonPourRafraichissement(JetonOAuth jetonOAuth) {
        if (ObjectUtils.isEmpty(jetonOAuth.getJetonRafraichissement())) {
            throw new RuntimeException("Le jeton de rafraichissement OAuth 2.0 est vide.");
        }

        if (config.scopeObligatoire && ObjectUtils.isEmpty(jetonOAuth.getScope())) {
            throw new RuntimeException("Le scope du jeton OAuth 2.0 est vide.");
        }
    }

    @Override
    public String getUrlDeconnexion(String urlRedirection) {
        Validate.isTrue(
                ObjectUtils.isEmpty(urlRedirection) || config.supporteRedirectionApresDeconnexion,
                "Ce client ne supporte pas la redirection après la déconnexion");

        UriBuilder builder = UriBuilder.fromUri(config.urlFermetureSession);

        if (!ObjectUtils.isEmpty(urlRedirection)) {
            builder.queryParam(NOM_PARAM_REQUETE_LOGOUT_CONTINUE_URL, urlRedirection);
        }

        ajouterQueryParamEtiquetteSiNecessaire(builder);

        return builder.build().toString();
    }

    @Override
    public JetonOAuth validerJeton(String token) {
        Assert.hasText(token, "Le token est obligatoire.");

        String uri = UriBuilder.fromUri(config.urlValidation)
                .build()
                .toString();

        RequestBuilder request = RequestBuilder.post()
                .setUri(uri)
                .setHeader(NOM_HEADER_AUTHORIZATION, construireEnteteBasicAuthentification())
                .addParameter(NOM_PARAM_REQUETE_VALIDATION_TOKEN, token)
                .addParameter(NOM_PARAM_REQUETE_API_KEY, config.idClient);
        ajouterHeaderEtiquetteSiNecessaire(request);

        try (CloseableHttpResponse response = httpClient.execute(request.build())) {

            HttpEntity entity = response.getEntity();

            String reponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);

            if (response.getStatusLine().getStatusCode() == 401) {
                throw new RuntimeException("Jeton invalide: " + response.getStatusLine().toString() + reponse);
            }

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode >= 300) {
                throw new RuntimeException("Erreur lors de la validation du token OAuth: " + response.getStatusLine().toString() + reponse);
            }

            JetonOAuth jeton = jsonValidationToJeton(token, reponse);

            jeton.valider();

            return jeton;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<String> getUrlRefreshSso() {
        return config.urlRefreshSso.map(this::ajouterQueryParamEtiquetteSiNecessaire);
    }

    private String ajouterQueryParamEtiquetteSiNecessaire(String url) {
        UriBuilder builder = UriBuilder.fromUri(url);
        ajouterQueryParamEtiquetteSiNecessaire(builder);
        return builder.build().toString();
    }

    private JetonOAuth executerRequeteToken(HttpUriRequest request, String grantType) {
        try (CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();

            String reponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode >= 300) {
                String message = String.format(
                        "Erreur lors de l'execution de la requete token pour le grant type %s : %s %s",
                        grantType,
                        response.getStatusLine().toString(),
                        reponse);
                throw new RuntimeException(message);
            }

            return jsonToJeton(config.idClient, reponse);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected JetonOAuth jsonValidationToJeton(String token, String rawJsonValidationResult) {
        Assert.notNull(rawJsonValidationResult, "rawJsonValidationResult is null");
        Assert.notNull(token, "token is null");

        JsonObject json = new JsonParser().parse(rawJsonValidationResult).getAsJsonObject();

        String result = extraireValeurJson(ATTRIBUT_REPONSE_VALIDATION_RESULT, json);

        if (!VALEUR_ATTRIBUT_REPONSE_VALIDATION_RESULT_VALID.equals(result)) {
            throw new RuntimeException(token);
        }

        String clientId = extraireValeurJson(ATTRIBUT_REPONSE_CLIENT_ID, json);
        String username = extraireValeurJson(ATTRIBUT_REPONSE_USER_NAME, json);
        String userid = extraireValeurJson(ATTRIBUT_REPONSE_USER_ID, json);
        String scope = extraireValeurJson(ATTRIBUT_REPONSE_SCOPE, json);
        String expiration = extraireValeurJson(ATTRIBUT_REPONSE_EXPIRATION, json);
        Date dateExpiration = new Date(Long.parseLong(expiration) * 1000);
        TypeJetonAcces typeJetonAcces = resoudreTypeJetonAcces(extraireValeurJson(ATTRIBUT_REPONSE_TOKEN_TYPE, json));
        TypeCompte typeCompte = resoudreTypeCompte(extraireValeurJson(ATTRIBUT_REPONSE_RESOURCE_OWNER_TYPE, json));

        return new JetonOAuth.Builder()
                .idUtilisateur(userid)
                .codeUtilisateur(username)
                .dateExpiration(dateExpiration)
                .identifiantClient(clientId)
                .jetonAcces(token)
                .typeCompte(typeCompte)
                .scope(scope)
                .typeJetonAcces(typeJetonAcces)
                .build();
    }

    /**
     * Crée un AccessToken à partir d'un Json.
     *
     * @param clientId     Le clientId to token
     * @param rawJsonToken Contenu du Json.
     * @return Le token
     */
    private JetonOAuth jsonToJeton(String clientId, String rawJsonToken) {

        Assert.notNull(rawJsonToken, "rawJsonToken is null");

        JsonObject json = new JsonParser().parse(rawJsonToken).getAsJsonObject();

        String token = extraireValeurJson(ATTRIBUT_REPONSE_TOKEN, json);
        String expiresIn = extraireValeurJson(ATTRIBUT_REPONSE_EXPIRES_IN, json);
        String refreshToken = extraireValeurJson(ATTRIBUT_REPONSE_REFRESH_TOKEN, json);
        String scope = extraireValeurJson(ATTRIBUT_REPONSE_SCOPE, json);
        String codeAcces = extraireValeurJson(config.attributReponseCodeUtilisateur, json);
        String userid = extraireValeurJson(ATTRIBUT_REPONSE_USER_ID, json);
        TypeCompte typeCompte = resoudreTypeCompte(extraireValeurJson(ATTRIBUT_REPONSE_RESOURCE_OWNER_TYPE, json));

        Date dateExpiration = null;

        if (expiresIn != null) {
            dateExpiration = new Date(System.currentTimeMillis() + Long.parseLong(expiresIn) * 1000);
        }

        return new JetonOAuth.Builder()
                .idUtilisateur(userid)
                .codeUtilisateur(codeAcces)
                .dateExpiration(dateExpiration)
                .identifiantClient(clientId)
                .jetonAcces(token)
                .jetonRafraichissement(refreshToken)
                .typeCompte(typeCompte)
                .scope(scope)
                .build();
    }

    /**
     * Extraire la valeur correpondante à un élément d'un Json.
     *
     * @param nomElementJson Élément pour lequel la valeur sera extraite.
     * @param jsonObject     Json.
     * @return Valeur extraite de l'élément ou null si l'élément n'existe pas dans le Json.
     */
    private static String extraireValeurJson(String nomElementJson, JsonObject jsonObject) {
        JsonElement element = jsonObject.get(nomElementJson);

        return element == null ? null : element.getAsString();
    }

    private TypeCompte resoudreTypeCompte(String typeCompte) {
        if (typeCompte == null) {
            return config.typeCompteDefaut;
        }

        switch (typeCompte) {
            case VALEUR_RESOURCE_OWNER_TYPE_UTILISATEUR:
                return TypeCompte.UTILISATEUR;

            case VALEUR_RESOURCE_OWNER_TYPE_APPLICATION:
                return TypeCompte.APPLICATION;

            default:
                return config.typeCompteDefaut;
        }
    }

    private static TypeJetonAcces resoudreTypeJetonAcces(String typeJetonAcces) {
        if (typeJetonAcces == null) {
            throw new IllegalArgumentException("Le type de jeton d'accès ne peut pas être nul.");
        }

        switch (typeJetonAcces) {
            case VALEUR_TYPE_JETON_ACCES_BEARER:
                return TypeJetonAcces.BEARER;

            case VALEUR_TYPE_JETON_ACCES_MAC:
                return TypeJetonAcces.MAC;

            default:
                throw new IllegalArgumentException("Le type de jeton d'accès " + typeJetonAcces + " n'est pas supporté.");
        }
    }

    /**
     * Obtenir la valeur pour le authorization header lorsque on authentifie avec les credentials du client.
     *
     * @return Le authorisation header pour authentifier le client encode en Base64.
     */
    private String construireEnteteBasicAuthentification() {
        try {
            String userIdEffectif = config.authentificationBasicUrlEncoded ? URLEncoder.encode(config.idClient, config.authentificationBasicCharset) : config.idClient;
            String pwdEffectif = config.authentificationBasicUrlEncoded ? URLEncoder.encode(config.secretClient, config.authentificationBasicCharset) : config.secretClient;
            String identification = userIdEffectif + ":" + pwdEffectif;

            return "Basic " + Base64Utils.encodeToString(identification.getBytes(config.authentificationBasicCharset));

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private void ajouterHeaderEtiquetteSiNecessaire(RequestBuilder requestBuilder) {
        if (!ObjectUtils.isEmpty(config.etiquetteRequete)) {
            requestBuilder.addHeader(NOM_PARAM_REQUETE_ETIQUETTE, config.etiquetteRequete);
        }
    }

    private void ajouterQueryParamEtiquetteSiNecessaire(UriBuilder uriBuilder) {
        if (!ObjectUtils.isEmpty(config.etiquetteRequete)) {
            uriBuilder.queryParam(NOM_PARAM_REQUETE_ETIQUETTE, config.etiquetteRequete);
        }
    }

}

