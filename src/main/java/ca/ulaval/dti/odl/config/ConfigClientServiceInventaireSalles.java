package ca.ulaval.dti.odl.config;

public class ConfigClientServiceInventaireSalles {

    /**
     * L'url de base du serveur.
     */
    public final String urlServeur;

    /**
     * La partie à ajouter à l'url de base pour le service d'inventaire de salles.
     */
    public final String cheminInventaireSalles;

    /**
     * Le scope à utiliser lors de l'authentification du client pour accéder au service d'inventaire.
     */
    public final String scope;

    /**
     * L'id de client à utiliser dans la requête pour accéder au service d'inventaire.
     */
    public final String idClient;

    public ConfigClientServiceInventaireSalles(String urlServeur, String cheminInventaireSalles, String scope, String idClient) {
        this.urlServeur = urlServeur;
        this.cheminInventaireSalles = cheminInventaireSalles;
        this.scope = scope;
        this.idClient = idClient;
    }
}

