package ca.ulaval.dti.odl.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "odl.oauth2")
public class ConfigOAuthProperties {

    private String clientId;
    private String clientSecret;
    private String serveurUrl;


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getServeurUrl() {
        return serveurUrl;
    }

    public void setServeurUrl(String serveurUrl) {
        this.serveurUrl = serveurUrl;
    }
}



