package ca.ulaval.dti.odl.config;

import ca.ulaval.dti.odl.services.ClientServiceInventaireSalles;
import ca.ulaval.dti.odl.services.ClientServiceInventaireSallesImpl;
import ca.ulaval.dti.odl.services.salles.SalleGlo;
import ca.ulaval.dti.odl.utils.Reference;
import ca.ulaval.dti.odl.utils.ReferenceSimple;

import java.io.IOException;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 */
public class ClientServiceInventaireSallesHttpTest {
    private static final CloseableHttpClient HTTP_CLIENT = HttpClients.createDefault();
    private static final CloseableHttpAsyncClient HTTP_ASYNC_CLIENT = HttpAsyncClients.createDefault();
    private static final Reference<ConfigClientServiceInventaireSalles> CONFIG_SALLES = new ReferenceSimple<>(new ConfigClientServiceInventaireSalles("https://api.dev.ulaval.ca/",
            "glo/v1/api/reservation/lieux/salles",
            "glo-rv-api.lieux.trust",
            "l7xx7728c542e5ab402d985d60ef2d9c6404"));

    private static final ConfigClientOAuth CONFIG_OAUTH = ConfigClientOAuth.configClientOAuthOdl(
            "https://api.test.ulaval.ca/",
            "l7xx7728c542e5ab402d985d60ef2d9c6404",
            "ca0e7602000949e6971407eaed735a4a",
            "scope_test",
            null);

    @BeforeAll
    public static void init() {
        HTTP_ASYNC_CLIENT.start();
    }

    @AfterAll
    public static void close() throws IOException {
        HTTP_CLIENT.close();
        HTTP_ASYNC_CLIENT.close();
    }

    @Test
    public void obtenirInventaire_siConfigValide_alorsListeSalles(){
        // Étant donné
        ClientOAuth2Http clientOAuth2 = new ClientOAuth2Http(HTTP_CLIENT, HTTP_ASYNC_CLIENT, CONFIG_OAUTH);
        ClientServiceInventaireSalles client = new ClientServiceInventaireSallesImpl(clientOAuth2, HTTP_CLIENT, CONFIG_SALLES);

        // Quand
        List<SalleGlo> listeSalles = client.obtenirInventaire();

        // Alors
        assertNotNull(listeSalles);
        assertTrue(listeSalles.size() > 0);
    }
}
